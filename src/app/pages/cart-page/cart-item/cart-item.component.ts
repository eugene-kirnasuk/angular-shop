import {Component, Input, OnInit} from '@angular/core';
import {ShopItem} from '../../../shared/interfaces';
import {ShopService} from '../../../shared/services/shop.service';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit {

  @Input() item!: ShopItem;

  constructor(private shopService: ShopService) {
  }

  ngOnInit(): void {
  }

  minusCount() {
    this.shopService.removeItemFromCart(this.item.id);
  }

  plusCount() {
    this.shopService.addToCart(this.item.id);
  }

}
