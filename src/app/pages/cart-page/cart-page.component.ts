import {Component, OnInit} from '@angular/core';
import {ShopService} from '../../shared/services/shop.service';
import {HttpClient} from '@angular/common/http';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.scss']
})
export class CartPageComponent implements OnInit {


  constructor(public shopService: ShopService, private httpClient: HttpClient) {
  }

  readonly lettersRegex = '\^[a-zA-Z]+$';
  readonly phoneNumberRegex = '((\\+38)?\\(?\\d{3}\\)?[\\s\\.-]?(\\d{7}|\\d{3}[\\s\\.-]\\d{2}[\\s\\.-]\\d{2}|\\d{3}-\\d{4}))';

  userPurchaseForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.pattern(this.lettersRegex)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    phoneNumber: new FormControl('', [Validators.required, Validators.pattern(this.phoneNumberRegex)]),
    city: new FormControl('', [Validators.required, Validators.pattern(this.lettersRegex)]),
    address: new FormControl('', Validators.required)
  });

  ngOnInit(): void {
  }

  submitForm() {
    this.shopService.submitPurchase(this.userPurchaseForm.value);
  }

  clearCart() {
    this.shopService.clearCart();
  }

}
