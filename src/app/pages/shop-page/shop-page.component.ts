import {Component, OnInit} from '@angular/core';
import {ShopService} from '../../shared/services/shop.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop-page.component.html',
  styleUrls: ['./shop-page.component.scss']
})
export class ShopPageComponent implements OnInit {
  constructor(public shopService: ShopService) {
  }

  query: any = '';


  ngOnInit(): void {
  }

}
