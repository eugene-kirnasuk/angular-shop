import {Component, Input, OnInit} from '@angular/core';
import {ShopItem} from '../../../shared/interfaces';
import {ShopService} from '../../../shared/services/shop.service';

@Component({
  selector: 'app-shop-item',
  templateUrl: './shop-item.component.html',
  styleUrls: ['./shop-item.component.scss']
})
export class ShopItemComponent implements OnInit {
  @Input() shopData!: ShopItem;
  itemCount: number = 0;

  constructor(private shopService: ShopService) {

  }

  ngOnInit(): void {
  }

  buyItems() {
    this.shopService.addToCart(this.shopData.id);
  }

}
