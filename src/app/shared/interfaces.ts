export interface ShopItem {
  id: number;
  title: string;
  imageURL: string;
  description: string;
  price: number;
  count: number;
}

export interface Cart {
  cartItems: ShopItem[];
  totalPrice: number;
  totalCount: number;
}
