import {Injectable} from '@angular/core';
import {Cart, ShopItem} from '../interfaces';
import {BehaviorSubject} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {SummaryDialogComponent} from '../components/summary-dialog/summary-dialog.component';
import {Route, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ShopService {
  readonly shopItems: Readonly<ShopItem[]> = [
    {
      id: 1,
      title: 'Cubic rubik',
      description: 'This is a cubic rubik',
      imageURL: './assets/shop-item-1.png',
      price: 200,
      count: 0
    },
    {
      id: 2,
      title: 'Football ball',
      description: 'This is a good football ball',
      imageURL: './assets/shop-item-2.png',
      price: 300,
      count: 0
    },
    {
      id: 3,
      title: 'Basketball ball',
      description: 'This is a good basketball ball',
      imageURL: './assets/shop-item-3.png',
      price: 300,
      count: 0
    },
    {
      id: 4,
      title: 'Tennis ball',
      description: 'This is a good tennis ball',
      imageURL: './assets/shop-item-4.png',
      price: 300,
      count: 0
    },

    {
      id: 5,
      title: 'Football ball',
      description: 'This is a good football ball',
      imageURL: './assets/shop-item-2.png',
      price: 300,
      count: 0
    },
    {
      id: 6,
      title: 'Tennis ball',
      description: 'This is a good tennis ball',
      imageURL: './assets/shop-item-4.png',
      price: 300,
      count: 0
    },
    {
      id: 7,
      title: 'Basketball ball',
      description: 'This is a good basketball ball',
      imageURL: './assets/shop-item-3.png',
      price: 300,
      count: 0
    },
    {
      id: 8,
      title: 'Cubic rubik',
      description: 'This is a cubic rubik',
      imageURL: './assets/shop-item-1.png',
      price: 200,
      count: 0
    },
    {
      id: 9,
      title: 'Tennis ball',
      description: 'This is a good tennis ball',
      imageURL: './assets/shop-item-4.png',
      price: 300,
      count: 0
    }, {
      id: 10,
      title: 'Cubic rubik',
      description: 'This is a cubic rubik',
      imageURL: './assets/shop-item-1.png',
      price: 200,
      count: 0
    },
    {
      id: 11,
      title: 'Football ball',
      description: 'This is a good football ball',
      imageURL: './assets/shop-item-2.png',
      price: 300,
      count: 0
    },
    {
      id: 12,
      title: 'Basketball ball',
      description: 'This is a good basketball ball',
      imageURL: './assets/shop-item-3.png',
      price: 300,
      count: 0
    },
    {
      id: 13,
      title: 'Tennis ball',
      description: 'This is a good tennis ball',
      imageURL: './assets/shop-item-4.png',
      price: 300,
      count: 0
    },

    {
      id: 14,
      title: 'Football ball',
      description: 'This is a good football ball',
      imageURL: './assets/shop-item-2.png',
      price: 300,
      count: 0
    },
    {
      id: 15,
      title: 'Tennis ball',
      description: 'This is a good tennis ball',
      imageURL: './assets/shop-item-4.png',
      price: 300,
      count: 0
    },
    {
      id: 16,
      title: 'Basketball ball',
      description: 'This is a good basketball ball',
      imageURL: './assets/shop-item-3.png',
      price: 300,
      count: 0
    },
    {
      id: 17,
      title: 'Cubic rubik',
      description: 'This is a cubic rubik',
      imageURL: './assets/shop-item-1.png',
      price: 200,
      count: 0
    },
    {
      id: 18,
      title: 'Tennis ball',
      description: 'This is a good tennis ball',
      imageURL: './assets/shop-item-4.png',
      price: 300,
      count: 0
    },
  ];

  private cartData: Cart = {
    cartItems: [],
    totalCount: 0,
    totalPrice: 0
  };

  cartData$: BehaviorSubject<Cart> = new BehaviorSubject<Cart>(this.cartData);

  constructor(private dialog: MatDialog, private router: Router) {
    if (localStorage.getItem('cartData') == null) {
      localStorage.setItem('cartData', JSON.stringify(this.cartData));
    }

    this.cartData = JSON.parse(localStorage.getItem('cartData')!);
    this.cartData$.next(this.cartData);

    this.cartData$.subscribe(cartData => {
      this.calculateTotalCount();
      this.calculateTotalPrice();
      localStorage.setItem('cartData', JSON.stringify(cartData));
      console.log('Cart data from subscribe:', cartData);
    });
  }

  submitPurchase(userData: any) {
    console.log('Purchase data:', userData);
    console.log('Purchase data:', this.cartData$.getValue());

    const dialogRef = this.dialog.open(SummaryDialogComponent, {
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.clearCart();
      this.router.navigateByUrl('/shop');
    });
  }

  addToCart(id: number) {
    const itemFromCart = {...this.cartData.cartItems.find(el => el.id === id)};
    if (this.isEmpty(itemFromCart)) {
      const shopItem: ShopItem | any = {...this.shopItems.find(el => el.id === id)};
      shopItem.count++;
      this.cartData.cartItems.push(shopItem);
    } else {
      this.cartData.cartItems.find(el => el.id === itemFromCart.id)!.count++;
    }
    this.cartData$.next(this.cartData);
  }


  removeItemFromCart(id: number) {
    this.cartData.cartItems.forEach((el, index) => {
      if (el.id === id) {
        el.count--;
        if (el.count === 0) {
          this.cartData.cartItems.splice(index, 1);
        }
      }
    });
    this.cartData$.next(this.cartData);
  }

  clearCart() {
    this.cartData.cartItems = [];
    this.cartData$.next(this.cartData);
  }

  private calculateTotalCount() {
    let totalCount: number = 0;
    this.cartData.cartItems.forEach(el => {
      totalCount += el.count;
    });
    this.cartData.totalCount = totalCount;
  }

  private calculateTotalPrice() {
    let totalPrice: number = 0;
    this.cartData.totalPrice = 0;
    this.cartData.cartItems.forEach(el => {
      totalPrice += el.count * el.price;
    });
    this.cartData.totalPrice = totalPrice;
  }

  private isEmpty(obj: object) {
    return Object.keys(obj).length === 0;
  }


}
