import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './shared/components/header/header.component';
import {ShopPageComponent} from './pages/shop-page/shop-page.component';
import {ShopItemComponent} from './pages/shop-page/shop-item/shop-item.component';
import {CartPageComponent} from './pages/cart-page/cart-page.component';
import {CartItemComponent} from './pages/cart-page/cart-item/cart-item.component';
import {HttpClientModule} from '@angular/common/http';
import {SearchFilterPipe} from './shared/search-filter.pipe';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SummaryDialogComponent} from './shared/components/summary-dialog/summary-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ShopPageComponent,
    ShopItemComponent,
    CartPageComponent,
    CartItemComponent,
    SearchFilterPipe,
    SummaryDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
