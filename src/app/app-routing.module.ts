import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ShopPageComponent} from './pages/shop-page/shop-page.component';
import {CartPageComponent} from './pages/cart-page/cart-page.component';

const routes: Routes = [
  {path: '',   redirectTo: 'shop', pathMatch: 'full'},
  {path: 'shop', component: ShopPageComponent},
  {path: 'cart', component: CartPageComponent},
  {path: '**', component: ShopPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
// - cp dist/angular-shop/index.html dist/angular-shop/404.html
